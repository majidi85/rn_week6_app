import React from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import { WARNA_AKTIF } from '../../utils/constant';
import ButtonIcon from '../ButtonIcon';
import Gap from '../Gap';

const Saldo = () => {
  return (
    <View style={styles.container}>
        <View style={styles.infosaldo}>  
            <View style={styles.text}>
                <Text style={styles.lblsaldo}>Saldo : </Text>
                <Text style={styles.valuesaldo}>Rp. 100.000</Text>
            </View>
            <View style={styles.text}>
                <Text style={styles.lblpoint}>Antar Point</Text>
                <Text style={styles.valuepoint}>100 pints</Text>
            </View>
        </View>
        <View style={styles.btn_Aksi}>
            <ButtonIcon tittle="Add Saldo"/>
            {/* <Gap width={5} /> */}
            <ButtonIcon tittle="Get Point"/>
        </View>
    </View>
  );
};

export default Saldo;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    marginHorizontal: 25,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
    marginTop: -windowHeight*0.07,
    flexDirection:'row',
  },
  text: {
      flexDirection:'row',
      justifyContent:'space-between',
  },
  infosaldo:{
      width: '60%',
  },
  lblsaldo:{
    fontSize:20,
    fontFamily: 'TitilliumWeb-Regular',
  },
  valuesaldo: {
    fontSize:20,
    fontFamily: 'TitilliumWeb-Bold',
  },
  lblpoint:{
    fontSize:12,
    fontFamily: 'TitilliumWeb-Regular',
  },
  valuepoint: {
    fontSize:12,
    fontFamily: 'TitilliumWeb-Bold',
    color: WARNA_AKTIF
  },
  btn_Aksi:{
      flex:1,
      flexDirection:'row',
      justifyContent:'flex-end',
  }
});
