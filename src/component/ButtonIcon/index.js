import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import {IconAddSaldo, IconGetPoint, IconKiloan, IconKarpet, IconExpress, IconSatuan, IconSetrikaAja, IconVIP} from '../../assets'
import { WARNA_SEKUNDER } from '../../utils/constant'

const ButtonIcon = ({tittle, type}) => {
    const Icon = () => {
        if (tittle === "Add Saldo") return <IconAddSaldo />
        if (tittle === "Get Point") return <IconGetPoint />
        if (tittle === "Kiloan") return <IconKiloan />
        if (tittle === "Satuan") return <IconSatuan />
        if (tittle === "VIP") return <IconVIP />
        if (tittle === "Karpet") return <IconKarpet />
        if (tittle === "Setrika Aja") return <IconSetrikaAja />
        if (tittle === "Express") return <IconExpress />

        return <IconAddSaldo />
    }
    return (
        <TouchableOpacity style={styles.container(type)}>
            <View style={styles.button(type)}>
                <Icon />
            </View>
            <Text style={styles.text(type)}>{tittle}</Text>
            
        </TouchableOpacity>
    )
}

export default ButtonIcon

const styles = StyleSheet.create({
    container:(type) => ({
        marginBottom: type === "layanan" ? 12 : 0,
        marginRight: type === "layanan" ? 25 : 0,
    }),
    button:(type) => ({
        backgroundColor: WARNA_SEKUNDER,
        padding: type === "layanan" ? 12 : 7,
        borderRadius:10,
    }),
    text:(type) => ({
        fontSize:type === "layanan" ? 14 : 10,
        fontFamily:type === "layanan" ? 'TitilliumWeb-Light' : 'TitilliumWeb-Regular',
        textAlign:'center'
    }),
})
