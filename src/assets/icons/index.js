import IconAkun from './akun.svg';
import IconAkunActive from './akunActive.svg';
import IconHome from './home.svg';
import IconHomeActive from './homeActive.svg';
import IconPesanan from './pesanan.svg';
import IconPesananActive from './pesananActive.svg';
import IconAddSaldo from './addSaldo.svg';
import IconGetPoint from './getPoint.svg';
import IconExpress from './express.svg';
import IconKarpet from './karpet.svg';
import IconKiloan from './kiloan.svg';
import IconSatuan from './satuan.svg';
import IconSetrikaAja from './setrika.svg';
import IconVIP from './vip.svg';
import IconPesananAktif from './pesananAKtif.svg';

export {
    IconAkun, 
    IconAkunActive, 
    IconPesanan, 
    IconPesananActive, 
    IconHome, 
    IconHomeActive,
    IconAddSaldo,
    IconGetPoint,
    IconExpress,
    IconKarpet,
    IconKiloan,
    IconSatuan,
    IconSetrikaAja,
    IconVIP,
    IconPesananAktif,
}