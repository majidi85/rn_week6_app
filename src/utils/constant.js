export const WARNA_AKTIF = '#55C895';
export const WARNA_NonAKTIF = '#C8C8C8';
export const WARNA_SEKUNDER = '#E0F7EF';
export const WARNA_ABU = '#F6F6F6';
export const WARNA_WARNING = '#FF4D00';