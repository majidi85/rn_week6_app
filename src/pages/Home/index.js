import React from 'react'
import { Dimensions, ImageBackground, StyleSheet, Text, View,Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {ImageHeader, Logo} from '../../assets';
import {ButtonIcon, PesananAktif, Saldo} from '../../component';
import { WARNA_ABU } from '../../utils/constant';

const Home = () => {
    return (
        <View style={styles.page}>
            <ScrollView showsHorizontalScrollIndicator={false}>
            <ImageBackground source={ImageHeader} style={styles.header}>
                <Image source={Logo} style={styles.logo} />
                <View style={styles.hello}>
                    <Text style={styles.selamat}>Selamat Datang,</Text>
                    <Text style={styles.username}>Majidi</Text>
                </View>
            </ImageBackground>
            <Saldo /> 
            <View style={styles.layanan}>
                <Text style={styles.label}>Layanan Kami</Text></View> 
                <View style={styles.iconLayanan}>
                    <ButtonIcon tittle="Kiloan" type="layanan"/>
                    <ButtonIcon tittle="Satuan" type="layanan"/>
                    <ButtonIcon tittle="VIP" type="layanan"/>
                    <ButtonIcon tittle="Karpet" type="layanan"/>
                    <ButtonIcon tittle="Setrika Aja" type="layanan"/>
                    <ButtonIcon tittle="Express" type="layanan"/>
                </View>
                <View style={styles.pesananaktif}>
                    <Text style={styles.label}>Pesanan Aktif</Text>
                    <PesananAktif title="Pesanan No. 0002142" status="Sudah Selesai"/>
                    <PesananAktif title="Pesanan No. 0002142" status="Masih Dicuci"/>
                    <PesananAktif title="Pesanan No. 0002142" status="Sudah Selesai"/>
                    <PesananAktif title="Pesanan No. 0002142" status="Sudah Selesai"/>
                </View>
                </ScrollView>
        </View>
    )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    page: {
        flex:1,
        backgroundColor:'white',
    },
    header: {
        width: windowWidth,
        height: windowHeight*0.3,
        paddingHorizontal: 25,
        paddingTop:10
    },
    logo: {
        width:windowWidth*0.25,
        height:windowHeight*0.06
    },
    hello:{
        marginTop: windowHeight*0.030,
    },
    selamat:{
        fontSize: 24,
        fontFamily:'TitilliumWeb-Regular'
    },
    username:{
        fontSize:22,
        fontFamily:'TitilliumWeb-Bold'
    },
    layanan:{
        paddingLeft:25,
        paddingTop:15,
    },
    label:{
        fontSize: 18,
        fontFamily:'TitilliumWeb-Bold',
    },
    iconLayanan:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:10,
        flexWrap:'wrap',
        marginLeft:25,
    },
    pesananaktif: {
        paddingTop:10,
        paddingHorizontal:25,
        backgroundColor: WARNA_ABU,
        flex:1,
        borderTopRightRadius:20,
        borderTopLeftRadius:20,
    }
})
